require('dotenv').config()
const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');

app.use(cookieParser());

app.use( session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        httpOnly: true,
        maxAge: 3600000 // 1 Hour
    }
}));

app.use('/public', express.static( path.join(__dirname, 'www') ));

app.get('/', (req, res) => {
    console.log(req.session);
    if (req.session.counter == undefined) {
        res.redirect('/login');
    } else {
        res.redirect('./dashboard');
    }    
});

app.get('/create', (req, res)=>{
    req.session.counter = 0;
    res.send('Session was created...');
});

app.get('/login', (req, res)=>{
    res.send('Login screen');
});

app.get('/dashboard', (req, res)=>{
    req.session.counter += 1;
    res.send('Welcome to the dashboard');
});

app.listen(3000, () => console.log('App has started...'));